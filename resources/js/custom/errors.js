"use strict";

export default class Errors {
    constructor(...fields) {
        this.errors = {};
        fields.forEach(field => (this.errors[field] = []));
    }

    has(field) {
        return this.errors[field];
    }

    any() {
        for (let i = 0; i < Object.keys(this.errors).length; i++) {
            if (this.errors[Object.keys(this.errors)[i]].length > 0) {
                return true;
            }
        }
    }

    record(field, message) {
        if (!this.errors[field].includes(message)) {
            this.errors[field].push(message);
        }
    }

    clear(field) {
        if (field) {
            this.errors[field] = [];
            return;
        }
    }
}
