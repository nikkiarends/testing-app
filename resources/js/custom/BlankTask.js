"use strict";
export default class BlankProduct {
    constructor() {
        (this.name = undefined),
            (this.description = undefined),
            (this.content = undefined),
            (this.status = undefined);
    }
}
