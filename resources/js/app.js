"use strict";
require("./bootstrap");

import router from "./router";
import store from "./store";

window.addEventListener("load", function() {
    new Vue({
        el: "#app",

        data: {},

        methods: {},

        components: {},

        router,

        store,

        created() {
            this.$store.commit("loadTasks");
        }
    });
});
