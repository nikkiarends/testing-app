"use strict";
import VueRouter from "vue-router";
import indexTabs from "./indexTabs.js";

const router = new VueRouter({
    mode: "history",
    linkActiveClass: "is-active"
});

router.addRoutes(indexTabs);

export default router;
