import Show from "../components/tabs/Show";
import Edit from "../components/tabs/Edit";
import Create from "../components/tabs/Create";
import Index from "../components/tabs/Index";

export default [
    {
        path: "/tasks/index",
        component: Index
    },

    {
        path: "/tasks/create",
        component: Create
    },

    {
        path: "/tasks/edit",
        component: Edit
    },

    {
        path: "/tasks/show",
        component: Show
    }
];
