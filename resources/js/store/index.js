"use strict";

import Vue from "vue";
import Vuex from "vuex";
import BlankTask from "../custom/BlankTask.js";
import axios from "axios";
import { timingSafeEqual } from "crypto";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        message: "Er zijn geen nieuwe boodschappen",
        tasks: [],
        editTask: new BlankTask(),
        showTask: new BlankTask()
    },

    mutations: {
        loadTasks(state) {
            axios
                .get("/api/tasks/all")
                .then(response => (state.tasks = response.data));
        },

        updateTask(state, updatedTask) {
            const oldTask = state.tasks.find(p => p.id == updatedTask.id);
            axios
                .put("/api/tasks/" + updatedTask.id + "/edit", updatedTask)
                .then(
                    response => (
                        Object.assign(oldTask, response.data.data),
                        (state.message = response.data.message)
                    )
                );
        },

        deleteTask(state, task) {
            axios
                .get("/api/tasks/" + task.id + "/delete")
                .then(
                    response => (
                        (state.message = response.message),
                        state.tasks.splice(state.tasks.indexOf(task), 1)
                    )
                );
        },

        saveTask(state, task) {
            axios
                .post("/api/tasks/new", {
                    name: task.name,
                    description: task.description,
                    content: task.content
                })
                .then(
                    response => (
                        state.tasks.push(response.data.data),
                        (state.message = response.data.message)
                    )
                );
        },

        toggleStatus(state, task) {
            const oldTask = state.tasks.find(p => p.id == task.id);
            task.status += 1;
            if (task.status > 3) {
                task.status = 1;
            }
            axios
                .put("/api/tasks/" + task.id + "/edit", task)
                .then(
                    response => (
                        Object.assign(oldTask, response.data.data),
                        (state.message = response.data.message)
                    )
                );
        },

        setEditTask(state, task) {
            state.editTask = { ...task };
        },

        setShowTask(state, task) {
            state.showTask = { ...task };
        },

        resetEditTask: state => {
            state.editTask = new BlankTask();
        }
    },

    getters: {
        getMessage: state => {
            return state.message;
        },

        getEditTask: state => {
            return state.editTask;
        },

        getShowTask: state => {
            return state.showTask;
        },

        allTasks: state => {
            return state.tasks;
        }
    }
});
