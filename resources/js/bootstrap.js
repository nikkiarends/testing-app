window._ = require("lodash");

//jQuery & Bootstrap

try {
    window.Popper = require("popper.js").default;
    window.$ = window.jQuery = require("jquery");

    require("bootstrap");
} catch (e) {}

// axios
window.axios = require("axios");

window.axios.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";

//  CSRF
let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common["X-CSRF-TOKEN"] = token.content;
} else {
    console.error(
        "CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token"
    );
}

// Vue
import Vue from "vue";
import VueRouter from "vue-router";
import Vuex from "vuex";

window.Vue = Vue;
Vue.use(VueRouter);
Vue.use(Vuex);

Vue.config.devtools = true;
Vue.config.productionTip = false;
