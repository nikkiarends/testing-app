<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Task;
use Faker\Generator as Faker;

$factory->define(Task::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->text($maxNbChars = 50),
        'content' => $faker->text($maxNbChars = 450),
        'status' => rand(1, 3),
        'created_at' => now(),
    ];
});
