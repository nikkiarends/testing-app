import {
    mount,
    createLocalVue
}
from 'vue-test-utils';
import Vuex from 'vuex'
import expect from 'expect';
import Show from "../resources/js/components/tabs/Show.vue";
import store from "../resources/js/store";

const localVue = createLocalVue()
localVue.use(Vuex)


describe("Show", () => {
    let wrapper;

    beforeEach(() => {
        wrapper = mount(Show, {
            store,
            localVue,
        });
        setTasks()
    });

    it('shows a select menu for a task', () => {
        expect(wrapper.contains("select.custom-select")).toBe(true)
    });

    it('shows all tasks in the select', () => {
        setTasks()
        see(store.state.tasks[0])
        see(store.state.tasks[1])
    });

    it('sets the task in the select', () => {
        setTaskToBe1() //you select task 1
        expect(wrapper.vm.chosenTask.id).toBe("1")
    })


    it('sets the right task from the store', () => {
        setTaskToBe1()
        let task = wrapper.vm.chosenTask
        expect(task.id).toBe(store.state.tasks[0].id)
        expect(task.name).toBe(store.state.tasks[0].name)
        expect(task.description).toBe(store.state.tasks[0].description)
    })

    it("shows the selected task information", () => {
        setTasks()
        setTaskToBe1()
        let task = wrapper.vm.chosenTask
        see(task.id)
        see(task.name)
        see(task.description)
        see(task.content)
        see(task.status)
    });



    //Helper functions
    let see = (text, selector) => {
        let wrap = selector ? wrapper.find(selector) : wrapper;
        expect(wrap.html()).toContain(text);
    };

    function setTasks() {
        store.state.tasks = [{
                id: '1',
                name: 'task1',
                description: 'task1 description',
                content: "abcde",
                status: 1,
            },
            {
                id: '2',
                name: 'task2',
                description: 'task2 description',
                content: "abcde",
                status: 2
            }
        ]
    }

    function setTaskToBe1() {
        const select = wrapper.find('select.custom-select')
        select.element.value = store.state.tasks[0]
        select.trigger('click')
    }
});
