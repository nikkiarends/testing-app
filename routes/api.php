<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::get('/tasks/all', 'TaskController@all');
Route::post('/tasks/new', 'TaskController@store');
Route::put('tasks/{id}/edit', 'TaskController@update');
Route::delete('tasks/{id}/delete', 'TaskController@destroy');