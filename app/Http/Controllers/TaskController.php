<?php

namespace App\Http\Controllers;

use App\Http\Requests\DestroyTaskRequest;
use App\Http\Requests\EditTaskRequest;
use App\Http\Requests\StoreTaskRequest;
use App\Http\Resources\Task as TaskResource;
use App\Task;

class TaskController extends Controller
{
    public function all()
    {
        $tasks = Task::All();
        foreach ($tasks as $task) {
            $task['condition'] = $task->condition();
            array_push($statusArray, $task->status);
        }

        return $tasks ;
    }

    public function store(StoreTaskRequest $request)
    {
        $input = $request->validated();

        $newTask = Task::create($input);

        // TODO:: find more elegant way to add the default status to the returned information
        $task = Task::find($newTask->id);

        return (new TaskResource(($task)))->additional(['message' => "Taak $task->name aangemaakt"]);
    }

    public function update(EditTaskRequest $request, Task $task)
    {
        $input = $request->validated();

        $updatedTask = Task::find($request->id)->fill($input);
        $updatedTask->status = $input['status'];
        $updatedTask->save();

        return (new TaskResource(($updatedTask)))->additional(['message' => "Taak $updatedTask->name aangepast"]);
    }

    public function destroy(DestroyTaskRequest $request, Task $task)
    {
        Task::destroy($request->id);

        return ["message" => "Taak verwijderd"];
    }
}
