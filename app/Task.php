<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = ['name', 'description', 'content'];

    // TODO:: implement enums for task-status
    public function condition()
    {
        if ($this->status === 1) {
            return "Nieuwe Taak";
        }
        if ($this->status === 2) {
            return "Mee Bezig";
        }
        if ($this->status === 3) {
            return "Klaar";
        }
    }
}
